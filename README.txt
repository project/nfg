Description
-----------
This module provides functionality to connect to the Network for Good credit
card processing API.

Requirements
------------
Drupal 7.x

Installation
------------
To install Drupal modules, see https://drupal.org/documentation/install/modules-themes/modules-7

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/nfg