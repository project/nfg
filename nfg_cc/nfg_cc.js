/**
 * @file
 * Provides JavaScript additions to the Network for Good credit card fields.
 *
 * This file provides progress bar support (if available), popup windows for
 * file previews, and disabling of other file fields during Ajax uploads (which
 * prevents separate file fields from accidentally uploading files).
 */

(function ($) {



/**
 * Attach behaviors to the file upload and remove buttons.
 */
Drupal.behaviors.nfgCreditEdit = {
  attach: function (context) {

    $('input.nfg-cc-change-toggle', context).bind("change", function() {
      var val = $(this).val();
      $(this).parents('.form-type-nfg-cc-number').toggleClass('nfg-hide', val==1);
      $(this).parents('.form-type-nfg-cc-number').toggleClass('nfg-hide-all', val==2);
    });
  },
};


})(jQuery);